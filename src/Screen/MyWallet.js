import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconEye from 'react-native-vector-icons/FontAwesome5';

import { StyleSheet, Text, View, Image, ScrollView, ImageBackground, Dimensions } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const MyWallet = () => {
    return (
        <>
            <ScrollView style={{ backgroundColor: 'white' }}>
                <View style={styles.wrapper}>
                    <LinearGradient colors={['#B22F34', '#A22C30', '#A6282C']} style={styles.linearGradient}>
                        <View style={styles.header}>
                            <View style={{ flex: 1 }}></View>
                            <View style={{ flex: 8, alignItems: 'center' }}>
                                <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold', }}>Ví của tôi</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                                <Icon name='credit-card' size={22} color={'white'} />
                            </View>
                        </View>
                        <View style={styles.contentHeader}>
                            <View style={styles.boxText}>
                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.colorText}>
                                        Số dư ví
                                </Text>
                                    <IconEye style={{ color: 'white', opacity: 0.7 }} name="eye-slash" size={16} />
                                </View>

                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={styles.colorPrice}>
                                        50,000
                                    </Text>
                                    <Text style={{ color: '#cccccc', fontSize: 30, marginLeft: 5, marginBottom: 20 }}>đ</Text>
                                </View>
                            </View>
                        </View>
                    </LinearGradient>
                    <View style={styles.listIcon}>
                        <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                            <Image style={styles.img} source={require('../Images/images/ok.jpg')} />
                            <Text style={styles.listIconText}>Nạp tiền </Text>
                        </View>
                        <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                            <Image style={styles.img} source={require('../Images/images/ok.jpg')} />
                            <Text style={styles.listIconText}>Rút tiền</Text>
                        </View>
                        <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                            <Image style={styles.img} source={require('../Images/images/ok.jpg')} />
                            <Text style={styles.listIconText}>Chuyển tiền</Text>
                        </View>
                        <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                            <Image style={styles.img} source={require('../Images/images/ok.jpg')} />
                            <Text style={styles.listIconText}>Thanh Toán </Text>
                        </View>
                    </View>
                </View>
                <View style={styles.group}>
                    <Text style={styles.groupText}>Quỹ Nhóm</Text>
                    <View>
                        <View style={{ flexDirection: 'row', flex: 1, marginTop: 10 }}>
                            <View style={{
                                borderRadius: 20,
                                paddingRight: 15,
                            }}>
                                <ImageBackground
                                    source={require('../Images/images/123ok.jpg')}
                                    imageStyle={{ borderRadius: 10 }}
                                    style={{
                                        borderRadius: 10,
                                        flex: 1,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                            width: 0,
                                            height: 2,
                                        },
                                        shadowOpacity: 0.25,
                                        shadowRadius: 3.84,

                                        elevation: 10,
                                    }}
                                >
                                    <View style={{}}>
                                        <View style={{
                                            justifyContent: 'center', paddingVertical: 10,
                                        }}>
                                            <View style={{ alignItems: 'center', backgroundColor: '#054cfa', justifyContent: 'center', marginLeft: 10, width: 30, height: 30, borderRadius: 30, borderColor: 'white', borderWidth: 4 }}>
                                                <Icon style={{ color: 'white', }} name='plus' size={18} />
                                            </View>
                                        </View>
                                        <View style={{
                                            backgroundColor: 'white', paddingVertical: 10, width: (windowWidth - 60) / 2,
                                            borderBottomLeftRadius: 10,
                                            borderBottomRightRadius: 10,

                                        }}>
                                            <Text style={{ fontSize: 16, marginLeft: 10 }}>
                                                Gia đình
                                        </Text>
                                        </View>
                                    </View>
                                </ImageBackground>
                            </View>
                            <View style={{ borderRadius: 20 }}>
                                <ImageBackground
                                    source={require('../Images/images/123ok.jpg')}
                                    imageStyle={{ borderRadius: 10 }}
                                    style={{
                                        borderRadius: 10,
                                        flex: 1,
                                        width: 170,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                            width: 0,
                                            height: 2,
                                        },
                                        shadowOpacity: 0.25,
                                        shadowRadius: 3.84,

                                        elevation: 10,
                                    }}
                                >
                                    <View style={{}}>
                                        <View style={{
                                            justifyContent: 'center', paddingVertical: 10,
                                        }}>
                                            <View style={{ alignItems: 'center', backgroundColor: '#054cfa', justifyContent: 'center', marginLeft: 10, width: 30, height: 30, borderRadius: 30, borderColor: 'white', borderWidth: 4 }}>
                                                <Icon style={{ color: 'white', }} name='plus' size={18} />
                                            </View>
                                        </View>
                                        <View style={{
                                            backgroundColor: 'white', paddingVertical: 10,
                                            borderBottomLeftRadius: 10,
                                            borderBottomRightRadius: 10,
                                        }}>
                                            <Text style={{ fontSize: 16, marginLeft: 10 }}>
                                                Đồng nghiệp
                                        </Text>
                                        </View>
                                    </View>
                                </ImageBackground>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', flex: 1, marginTop: 18 }}>
                            <View style={{
                                borderRadius: 20, paddingRight: 15,
                            }}>
                                <ImageBackground
                                    source={require('../Images/images/123ok.jpg')}
                                    imageStyle={{ borderRadius: 10 }}
                                    style={{
                                        borderRadius: 10,
                                        flex: 1,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                            width: 0,
                                            height: 2,
                                        },
                                        shadowOpacity: 0.25,
                                        shadowRadius: 3.84,

                                        elevation: 10,
                                    }}
                                >
                                    <View style={{}}>
                                        <View style={{
                                            justifyContent: 'center', paddingVertical: 10,
                                        }}>
                                            <View style={{ alignItems: 'center', backgroundColor: '#054cfa', justifyContent: 'center', marginLeft: 10, width: 30, height: 30, borderRadius: 30, borderColor: 'white', borderWidth: 4 }}>
                                                <Icon style={{ color: 'white', }} name='plus' size={18} />
                                            </View>
                                        </View>
                                        <View style={{
                                            backgroundColor: 'white', paddingVertical: 10, width: (windowWidth - 60) / 2,
                                            borderBottomLeftRadius: 10,
                                            borderBottomRightRadius: 10,

                                        }}>
                                            <Text style={{ fontSize: 16, marginLeft: 10 }}>
                                                Gia đình
                                        </Text>
                                        </View>
                                    </View>
                                </ImageBackground>
                            </View>
                            <View style={{ borderRadius: 20 }}>
                                <ImageBackground
                                    source={require('../Images/images/123ok.jpg')}
                                    imageStyle={{ borderRadius: 10 }}
                                    style={{
                                        borderRadius: 10,
                                        flex: 1,
                                        width: 170,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                            width: 0,
                                            height: 2,
                                        },
                                        shadowOpacity: 0.25,
                                        shadowRadius: 3.84,
                                        elevation: 10,
                                    }}
                                >
                                    <View style={{}}>
                                        <View style={{
                                            justifyContent: 'center', paddingVertical: 10,
                                        }}>
                                            <View style={{ alignItems: 'center', backgroundColor: '#054cfa', justifyContent: 'center', marginLeft: 10, width: 30, height: 30, borderRadius: 30, borderColor: 'white', borderWidth: 4 }}>
                                                <Icon style={{ color: 'white', }} name='plus' size={18} />
                                            </View>
                                        </View>
                                        <View style={{
                                            backgroundColor: 'white', paddingVertical: 10,
                                            borderBottomLeftRadius: 10,
                                            borderBottomRightRadius: 10,
                                        }}>
                                            <Text style={{ fontSize: 16, marginLeft: 10 }}>
                                                Đồng nghiệp
                                        </Text>
                                        </View>
                                    </View>
                                </ImageBackground>
                            </View>
                        </View>

                    </View>
                </View>
                <View style={{ marginTop: 20, paddingHorizontal: 20 }}>
                    <Text style={styles.textHistory}>Lịch sử giao dịch</Text>
                    <View style={{ flexDirection: 'row', paddingBottom: 10, alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#cccccc' }}>
                        <View style={{ flex: 1, alignItems: 'center', backgroundColor: '#054cfa', justifyContent: 'center', width: 40, height: 40, borderRadius: 40, borderColor: 'white', borderWidth: 4 }}>
                            <IconEye style={{ color: 'white', }} name='shopping-cart' size={16} />
                        </View>
                        <View style={{ flex: 6, padding: 5 }}>
                            <Text numberOfLines={1}>Thánh toán
                                <Text style={{ fontWeight: 'bold', }}> VM + HNI Số 1 Tập 123211321</Text>
                            </Text>
                            <Text style={{ color: 'gray', paddingVertical: 5 }}>
                                06/09/2020 - 15:27
                            </Text>
                        </View>
                        <View style={{ flex: 3, alignItems: 'flex-end' }}>
                            <Text>-189.300đ</Text>
                            <Text style={{ color: 'green' }}>Thành công</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', paddingBottom: 10, alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#cccccc' }}>
                        <View style={{ flex: 1, alignItems: 'center', backgroundColor: '#054cfa', justifyContent: 'center', width: 40, height: 40, borderRadius: 40, borderColor: 'white', borderWidth: 4 }}>
                            <IconEye style={{ color: 'white', }} name='shopping-cart' size={16} />
                        </View>
                        <View style={{ flex: 6, padding: 5 }}>
                            <Text numberOfLines={1}>Thánh toán
                                <Text style={{ fontWeight: 'bold', }}> VM + HNI Số 1 Tập 123211321</Text>
                            </Text>
                            <Text style={{ color: 'gray', paddingVertical: 5 }}>
                                06/09/2020 - 15:27
                            </Text>
                        </View>
                        <View style={{ flex: 3, alignItems: 'flex-end' }}>
                            <Text>-189.300đ</Text>
                            <Text style={{ color: 'green' }}>Thành công</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </>
    )
}
const styles = StyleSheet.create({
    wrapper: {
        position: 'relative',
        backgroundColor: 'white',
    },
    linearGradient: {
        paddingHorizontal: 20,
        paddingTop: 20,
        width: '100%',
        height: windowHeight / 3.5,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    header: {
        color: 'white',
        flexDirection: 'row',
    },
    contentHeader: {
        marginTop: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    colorText: {
        color: 'white',
        fontSize: 16,
        opacity: 0.7,
        paddingRight: 10
    },
    boxText: {

    },
    colorPrice: {
        color: 'white',
        fontSize: 36,
    },
    listIcon: {
        position: 'absolute',
        bottom: -60,
        marginHorizontal: '5%',
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 100,
        width: '90%',
        borderRadius: 10,
        paddingRight: 15,
        paddingLeft: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.51,
        shadowRadius: 13.16,

        elevation: 10,
    },
    img: {
        marginTop: 10,
        width: 50,
        height: 50,
        marginBottom: 10,
    },
    listIconText: {
        color: 'gray',
        fontSize: 14
    },
    group: {
        marginTop: 80,
        paddingHorizontal: 20,
    },
    groupText: {
        fontSize: 20,
        fontWeight: '700',
        marginBottom: 10,
    },
    textHistory: {
        fontSize: 18,
        fontWeight: '700',
        marginBottom: 10,
    },
})

export default MyWallet