import React, { useState } from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from "react-native-vector-icons/MaterialCommunityIcons";
import { Button, Image, StyleSheet, Text, View } from 'react-native';
import Modal from 'react-native-modal';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import ScanPay from './Scan/ScanPay';
import ScanCard from './Scan/ScanCard';
import ScanCode from './Scan/ScanCode';


const TabScan = createBottomTabNavigator ();

const Scan = ({navigation}) => {
    return(
        <>
           <View style={{flex: 1, backgroundColor:'red'}}>
           <View style={{ flex: 1, alignItems: 'stretch', margin: 20 }}>
               <View style={{flex: 1, flexDirection: 'row',justifyContent:'space-between'}}>
                    <View >
                        <Icon name='clear' onPress={() => navigation.goBack()} style={{fontSize: 30, color:'white'}} />
                    </View>
                    <View >
                        <Icon2 name='lightbulb-on-outline'  style={{fontSize: 30, color:'white'}} />
                    </View>
               </View>
               <View style={{flex: 13,justifyContent:'space-between'}}>
                   <TabScan.Navigator
                        tabBarOptions={{
                            labelStyle:{
                              fontSize: 20,
                              fontWeight: '600'
                            },
                            activeTintColor: 'white',
                            inactiveTintColor: 'gray',
                            shifting: false,
                            labeled: true,  
                            style:{backgroundColor:'red', textAlign:'center'}
                          }}
                   >
                       <TabScan.Screen name='Thanh toán' options={{tabBarLabel:'Thanh toán'}} component={ScanPay}></TabScan.Screen>
                       <TabScan.Screen name='Dùng thẻ' options={{tabBarLabel:'Dùng thẻ'}} component={ScanCard}></TabScan.Screen>
                       <TabScan.Screen name='Quét mã' options={{tabBarLabel:'Quét mã'}} component={ScanCode}></TabScan.Screen>
                   </TabScan.Navigator>
               </View>
            </View>
           </View>
        </>
    )
}

const styles = StyleSheet.create({
    Container:{
        flex: 1,
        backgroundColor: 'red',
    },
    button:{
        backgroundColor: 'transparent',
        borderRadius: 40,
        marginVertical: 10,
        paddingVertical: 16,
        width: 120,
        borderColor: 'white',
        borderWidth: 1,
    },
    buttonText:{
        fontSize : 20,
        fontWeight: '700',
        color: 'white',
        textAlign: 'center'
    }
})

export default Scan