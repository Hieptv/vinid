import React, { useState } from 'react';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import 'react-native-gesture-handler';
import {createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon3 from 'react-native-vector-icons/Fontisto';
import Icon4 from "react-native-vector-icons/MaterialIcons";
import Scan from './src/Screen/Scan';
import MyWallet from './src/Screen/MyWallet';
import Home from './src/Screen/Home';
import Account from './src/Screen/Account';
import MailBox from './src/Screen/MailBox';
import ScanNavigation from './src/StackNavigation/ScanNavigation';



const Tab = createBottomTabNavigator ();


const App = ({navigation}) => {
  
  return (
    <>
     <NavigationContainer >
      
        <Tab.Navigator
          tabBarOptions={{
            labelStyle:{
              fontSize: 13
            },
            activeTintColor: 'red',
            inactiveTintColor: 'gray',
            
          }}
        >
            <Tab.Screen 
              name="Trang Chủ" 
              component={ Home } 
              options={{tabBarLabel:'Trang chủ',tabBarIcon: ({color})=> <Icon3 name='home' size={25} color={color}></Icon3>}}
            >
            </Tab.Screen>
            <Tab.Screen 
              name = "Ví của tôi" 
              component ={MyWallet} 
              options={{tabBarLabel:'Ví của tôi',tabBarIcon: ({color})=> <Icon2 name='wallet' size={25} color={color}></Icon2>}}
            >
            </Tab.Screen>
            <Tab.Screen 
              listeners={({ navigation }) => ({
                tabPress: e => {
                  e.preventDefault();
                  navigation.navigate('Scan');
                },
              })}
              name = 'Scan' 
              component ={Scan} 
              options={{tabBarLabel:'',tabBarIcon: ()=> <Icon2  name='qrcode-scan' size={35} color='red' style={{marginTop: 15}}></Icon2>}}
            >
            </Tab.Screen>
            <Tab.Screen 
              name = 'hộp thư' 
              component ={MailBox } 
              options={{tabBarLabel:'Hộp thư',tabBarIcon: ({color})=> <Icon2 name='message-processing' size={25} color={color}></Icon2>}}
            >
            </Tab.Screen>
            <Tab.Screen 
              name = 'Tài khoản' 
              component ={Account} 
              options={{tabBarLabel:'Tài khoản',tabBarIcon: ({color})=> <Icon name='user-o' size={25} color={color}></Icon>}}
            >
            </Tab.Screen>
        </Tab.Navigator>
      </NavigationContainer >
    </>
  );
};


export default App;
