import React, { useState } from 'react';
import 'react-native-gesture-handler';
import { createStackNavigator, HeaderTitle } from '@react-navigation/stack';
import Scan from '../Screen/Scan';

const Stack = createStackNavigator ();

const ScanScreen = () => {
    return(
        <Stack.Navigator  mode="modal" headerMode='none'  >
            <Stack.Screen options={{title:'', headerShown: false}} name='Scan' component={Scan} ></Stack.Screen>
        </Stack.Navigator>
    )
}

export default ScanScreen